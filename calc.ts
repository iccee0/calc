export class Calculator {
  private notcalculating = true
  
  public constructor() { }

  public mal(x: number, y: number): number {
    var temp: number;
    temp = 0;
    if (x < 0) {
      if (y < 0) {
        temp = 0;
        for (var i = y; i < 0; i++) {
          temp++;
        }
        y = temp;
        temp = 0;
        for (var i = x; i < 0; i++) {
          temp++;
        }
        x = temp;
      } else {
        [x, y] = [y, x];
      }
    }
    temp = 0;
    for (var i = 0; i < x; i++) temp = temp + y;
    return temp;
  }

  public minus(x: number, y: number): number {

    var temp: number;
    var negativ: Boolean;
    negativ = false;
    temp = 0;
    if (x < y) {
      [x, y] = [y, x];
      negativ = true
    }
    for (var i = 0; x != y + i; i++) {
      temp++;
    }
    if (negativ) {
      temp = this.mal(temp, -1); //not cool
    }
    return temp;
  }

  public geteilt(x: number, y:number): number{

    var string: string;
    var  temp, diverenz, zehntel, hundertstel: number;
    var negativ: boolean
    negativ = false;
    temp = 0;
    diverenz = 0;
    if (x < 0||y < 0) {
      if (y < 0 && x < 0) { } else {
        negativ = true;
      }
      if (y < 0) {
        for (var i = y; i < 0; i++) {
          temp++;
        }
        y = temp;
        temp = 0;
      }
      if (x < 0) {
        for (var i = x; i < 0; i++) {
          temp++;
        }
        x = temp;
      }
    }
    temp = 0;
	  for(var i = 0; x > i;i = i + y){
      temp++;
    }
    if (i != x) {
      temp = this.minus(temp,1)
      if (this.notcalculating) {
        this.notcalculating = false;
        diverenz = this.minus(x, this.mal(temp, y))
        console.log(diverenz)
        zehntel = this.geteilt(this.mal(diverenz, 10), y)
        diverenz = this.minus(this.mal(zehntel+1,y), this.mal(zehntel, y))
        console.log(diverenz)
        hundertstel = this.geteilt(this.mal(diverenz, 10), y)
        string = temp.toString() + "." + zehntel.toString().charAt(0) + hundertstel.toString().charAt(0)
        temp = Number(string);
        this.notcalculating = true
      }
    }
    if (negativ) {
      temp = 0;
      string = "-" + temp.toString() //not cool
      temp = Number(string)
    }
    return temp
  }

  public plus(x: number, y: number): number {
    return x + y
  }

}
