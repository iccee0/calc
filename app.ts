import { Application } from "https://deno.land/x/abc@v1/mod.ts";

const app = new Application();

import { Calculator } from "./calc.ts"

const calc = new Calculator();
const PORT = 1111;

console.log("Listening on Port: 1111")
console.log("http://localhost:1111/")


app.file("/", "index.html");
app
  .get("plus/:x/:y", (c) => {
    const { x } = c.params;
    const { y } = c.params;
    return calc.plus(Number(x),Number(y));
  })
 .get("minus/:x/:y", (c) => {
    const { x } = c.params;
    const { y } = c.params;
    return calc.minus(Number(x),Number(y)); 
  })
  .get("geteilt/:x/:y", (c) => {
    const { x } = c.params;
    const { y } = c.params;
    return calc.geteilt(Number(x), Number(y));
  })
  .get("mal/:x/:y", (c) => {
    const { x } = c.params;
    const { y } = c.params;
    return calc.mal(Number(x), Number(y));
  })
  .start({ port: PORT });
